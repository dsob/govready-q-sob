GovReady-Q Documentation
========================

The GovReady-Q Compliance Server is an open source GRC platform for highly automated, user-friendly, self-service compliance assessments and documentation. It's perfect for DevSecOps.

GovReady-Q solves the painful compliance bottleneck of needing months to authorize applications that deploy and redeploy in minutes.

The code is open source, and [available on GitHub](https://github.com/GovReady/govready-q).

.. ATTENTION::
   GovReady-Q is in Beta. Suggested for DevSecOps early adopters needing Compliance-as-Code.

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   introduction.md
   Deploy.md
   Permissions.md
   Authoring.md
   Automation.md
   DataDesign.md
   Test.md
   
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


